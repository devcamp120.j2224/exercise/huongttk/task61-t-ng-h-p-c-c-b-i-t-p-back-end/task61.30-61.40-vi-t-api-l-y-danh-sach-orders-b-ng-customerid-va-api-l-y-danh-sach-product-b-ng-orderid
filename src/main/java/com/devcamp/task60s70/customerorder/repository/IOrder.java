package com.devcamp.task60s70.customerorder.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task60s70.customerorder.models.COrder;

public interface IOrder extends JpaRepository <COrder, Long> {
    COrder findById(long id);
}
