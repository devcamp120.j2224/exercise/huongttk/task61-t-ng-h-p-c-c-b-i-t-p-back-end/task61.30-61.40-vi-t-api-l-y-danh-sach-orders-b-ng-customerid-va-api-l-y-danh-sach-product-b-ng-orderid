package com.devcamp.task60s70.customerorder.controller;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task60s70.customerorder.models.COrder;
import com.devcamp.task60s70.customerorder.models.CProduct;
import com.devcamp.task60s70.customerorder.repository.IOrder;

@RestController
@CrossOrigin
@RequestMapping
public class CProductController {
    @Autowired
    IOrder orderRepository;
    @GetMapping("devcamp-products")
    public ResponseEntity <Set<CProduct>> getProductListByOrderId(@RequestParam(value = "orderId")long orderId){
        try {
            COrder vOrder = orderRepository.findById(orderId);
            if (vOrder != null) {
                return new ResponseEntity<>(vOrder.getProducts(), HttpStatus.OK);
            }else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        }catch(Exception ex){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
