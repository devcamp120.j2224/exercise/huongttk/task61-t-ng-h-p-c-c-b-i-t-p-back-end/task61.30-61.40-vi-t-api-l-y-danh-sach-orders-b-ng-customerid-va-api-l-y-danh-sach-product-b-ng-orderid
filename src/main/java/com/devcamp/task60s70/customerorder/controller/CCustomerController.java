package com.devcamp.task60s70.customerorder.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task60s70.customerorder.models.CCustomer;
import com.devcamp.task60s70.customerorder.repository.ICustomerRepository;

@RestController
@CrossOrigin
@RequestMapping
public class CCustomerController {
    @Autowired
    ICustomerRepository customerRepository;
    @GetMapping("devcamp-customers")
    public ResponseEntity <List<CCustomer>>getAllCar(){
        try {
            List<CCustomer> listCustomer = new ArrayList<CCustomer>();
            customerRepository.findAll().forEach(listCustomer :: add);
            return new ResponseEntity<>(listCustomer, HttpStatus.OK);
        }catch (Exception ex){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
